var clientesObtenidos;

function getClientes(){
var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
var request = new XMLHttpRequest();

request.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    clientesObtenidos = request.responseText;
    procesarClientes();
      console.log(request.responseText);
  }
}
request.open("GET", url, true);
request.send();
}

function procesarClientes(){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  var JSONClientes = JSON.parse(clientesObtenidos);

  var divTablaClientes = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  var encabezado=document.createElement("tr");
  var columna1=document.createElement("th");
  columna1.innerText="Nombre";
  encabezado.append(columna1);

  var columna2=document.createElement("th");
  columna2.innerText="Ciudad";
  encabezado.append(columna2);

  var columna3=document.createElement("th");
  columna3.innerText="Telefono";
  encabezado.append(columna3);

  var columna4=document.createElement("th");
  columna4.innerText="Pais";
  encabezado.append(columna4);

  tabla.appendChild(encabezado);

  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    var columnaTelefono = document.createElement("td");
    columnaTelefono.innerText = JSONClientes.value[i].Phone;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if (JSONClientes.value[i].Country == "UK") {
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    }
    else {
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }
    columnaBandera.appendChild(imgBandera);


    nuevaFila.append(columnaNombre);
    nuevaFila.append(columnaCiudad);
    nuevaFila.append(columnaTelefono);
    nuevaFila.append(columnaBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.append(tabla);

}
