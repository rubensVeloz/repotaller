var productosObtenidos;

function getProductos(){

  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState==4 && this.status==200){
      productosObtenidos=request.responseText;
      procesarProductos();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarProductos() {
 var JSONProductos = JSON.parse(productosObtenidos);

 var divTabla = document.getElementById("divTabla");
 var tabla = document.createElement("table");
 var tbody = document.createElement("tbody");
 tabla.classList.add("table");
 tabla.classList.add("table-striped");

 var encabezado=document.createElement("tr");
 var columna1=document.createElement("th");
 columna1.innerText="Producto";
 encabezado.append(columna1);

 var columna2=document.createElement("th");
 columna2.innerText="Precio";
 encabezado.append(columna2);

 var columna3=document.createElement("th");
 columna3.innerText="Stock";
 encabezado.append(columna3);

 tabla.appendChild(encabezado);

 for (var i = 0; i < JSONProductos.value.length; i++) {
   var nuevaFila = document.createElement("tr");

   var columnaNombre = document.createElement("td");
   columnaNombre.innerText = JSONProductos.value[i].ProductName;
   nuevaFila.append(columnaNombre);

   var columnaPrecio = document.createElement("td");
   columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
   nuevaFila.append(columnaNombre);

   var columnaStock = document.createElement("td");
   columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

   nuevaFila.append(columnaNombre);
   nuevaFila.append(columnaPrecio);
   nuevaFila.append(columnaStock);

   tbody.appendChild(nuevaFila);
 }
 tabla.appendChild(tbody);
 divTabla.append(tabla);
}
